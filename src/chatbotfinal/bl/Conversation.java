/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbotfinal.bl;

import chatbotfinal.dal.FileHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Tabish Ali
 */
public class Conversation {
    
    private ArrayList questions;
    private ArrayList answers;
    private final ArrayList tempQues = new ArrayList();
    private final ArrayList tempAns = new ArrayList();
    private final HashMap queAndAns = new HashMap();
    private final FileHandler fh = new FileHandler();
    
    public void readQuesAndAnsFromFile(){
        Object[] obj;
        obj = fh.readQAFile();
        questions = (ArrayList) obj[0];
        answers = (ArrayList) obj[1];
    }
    
    public String getAnswer(String question){
        
        findQuestion(question.toLowerCase());
        int size = tempQues.size(), minED = Integer.MAX_VALUE, temp;
        String que = "", ans = "I have no answer for this question.!!";
        
        for(int i = 0; i < size; ++i){
            temp = getEditDist(tempQues.get(i).toString(), question);            
            //System.out.println(tempQues.get(i));
            if(temp < minED){
                minED = temp;
                que = tempQues.get(i).toString();
                ans = tempAns.get(i).toString();
            }
        }
        fh.writeDataToFile(question, ans);
        return ans;
    }
    
    private void findQuestion(String str){
        tempQues.clear();
        tempAns.clear();
        String[] arr;
        String questionSave;
        String answerSave;
        for(int i=0;i<questions.size();i++){
            questionSave = questions.get(i).toString().toLowerCase();
            answerSave = answers.get(i).toString().toLowerCase();
            if(questionSave.contains(str.toLowerCase())){
                tempQues.add(questionSave);
                tempAns.add(answerSave);
            }
        }
        arr =  str.split(" ");
        if(tempQues.size()==0)
        {
            for(int i=0;i<questions.size();i++)
            {
                questionSave = questions.get(i).toString().toLowerCase();
                answerSave = answers.get(i).toString().toLowerCase();
                if(questionSave.contains(arr[0].toLowerCase())){
                    tempQues.add(questionSave);
                    tempAns.add(answerSave);
                }
            }
        }
    }
    
    private int getEditDist(String str1, String str2){
        
        int row = str1.length() + 1, col = str2.length() + 1;
        int[][] arr = new int[row][col];
        for(int i = 0 ; i < col - 1; ++i)
            arr[0][i] = i;
        
        for(int i = 0; i < row - 1; ++i)
            arr[i][0] = i;
        int min;
        for(int i = 1; i < row; ++i){
            for(int j = 1; j < col; ++j){
                min = Math.min(Math.min(arr[i-1][j], arr[i-1][j-1]), arr[i][j-1]);
                if(str1.charAt(i - 1) == str2.charAt(j - 1)){
                    arr[i][j] = min;
                }
                else{
                    arr[i][j] = min + 1;
                }
            }
        }
        return arr[row-1][col-1];
    }
    
    
    public void startChat(){
        Scanner scan = new Scanner(System.in);
        String userQue = "a";
        
        while(!userQue.equalsIgnoreCase("bye")){
            
            userQue = scan.nextLine();
            if(userQue.equalsIgnoreCase("bye")){
                continue;
            }
            System.out.println(getAnswer(userQue));
        }
    }
    
}
