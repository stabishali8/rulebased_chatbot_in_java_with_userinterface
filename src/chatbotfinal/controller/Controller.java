/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbotfinal.controller;

import chatbotfinal.bl.Conversation;

/**
 *
 * @author Tabish Ali
 */
public class Controller {
    
    Conversation conversation = new Conversation();

    public String processRequest(Object obj, int action){
        
        switch(action){
            case 1:
                conversation.readQuesAndAnsFromFile();
                break;
            case 2:
                return conversation.getAnswer(obj.toString());
                //convo.startChat();
            default:
                System.out.println("Invalid option.");
        }
        return "";
    }
}
